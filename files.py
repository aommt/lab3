import os
import config

def readFileToList(readFile):
    try:
        img = open(readFile, 'r')
        lst = list()
        for line in img:
            lst.extend(line.rstrip().split('\n'))

        # if (lst[0] != "P3"):
        #     return NotImplementedError

        return lst

    except IOError:
        print("An IOError has occurred!")
    finally:
        img.close()


def readGrayFileToList(readFile):
    try:
        img = open(readFile, 'r')
        lst = list()
        lst1 = list()
        for line in img:
            lst.extend(line.rstrip().split('\n'))

        lst1.append(str(lst[0]))
        lst1.append(str(lst[1]))
        lst1.append(str(lst[2]))
        for i in range(3, len(lst)):
            lst1.extend(lst[i].rstrip())


        return lst1

    except IOError:
        print("An IOError has occurred!")
    finally:
        img.close()

def sizeFile(readFile):
    try:
        if os.path.isfile(readFile):
            file_info = os.stat(readFile)
            return (file_info.st_size)

    except IOError:
        print("An IOError has occurred!")



def listToWriteFile(list, writeFile):
    img2 = open(config.testFilePath + writeFile, 'w')
    img2.write('\n'.join(list) + '\n')
    img2.close()

