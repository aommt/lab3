import numpy as np
import matplotlib.pyplot as plt

def printHistogram(colorlist, color):


    fig, ax2 = plt.subplots(figsize=(5, 3))

    x = np.linspace(0, 255,256)

    his = [0]*256

    for i in range(len(colorlist)):
        his [colorlist[i]]= his [colorlist[i]]+ 1
    ax2.title.set_text('Гистограмма для '+color)

    if (color == 'red'):
        ax2.plot(x, his, 'r-')
    if (color == 'green'):
        ax2.plot(x, his, 'g-')
    if (color == 'blue'):
        ax2.plot(x, his, 'b-')
    plt.show()