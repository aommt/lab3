from parse import parse

import numpy as np
import time
from math import radians



from config import testFilePath
from files import readFileToList, listToWriteFile, readGrayFileToList, sizeFile
from plot import printHistogram


def convert_bgr_2_YCbCr(list):
    newlist = []
    newlist.append("P3\n#\n")

    newlist.append(list[2] + "\n")
    newlist.append("255" + "\n")

    # gimp никак не мог понять, что это не YCbCr.
    # при необходимости верхние строки раскомментировать, получается светлорозоватое изображение
    # сейчас будет просто набор данных, без параметризации под изображение,
    # формат и имя получаемого файла задается в config.py

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G= int(list[i * 3 + 5])
        B= int(list[i * 3 + 6])

        Y = 0.299 * R + 0.587 * G + 0.114 * B
        Cb = 0.564*(B - Y)
        Cr =  0.713*(R - Y)
        newlist.append(str((Y)))
        newlist.append(str((Cb)))
        newlist.append(str((Cr)))
    return newlist



def convert_bgr_to_YCbCr(list, filename):
    newlist = []
    newlist.append("P3")
    newlist.append("#")
    newlist.append(list[2])
    newlist.append("255")

    newlistY = []
    newlistY.append("P2")
    newlistY.append("#")
    newlistY.append(list[2])
    newlistY.append("255")

    newlistCb = []
    newlistCb.append("P2")
    newlistCb.append("#")
    newlistCb.append(list[2])
    newlistCb.append("255")

    newlistCr = []
    newlistCr.append("P2")
    newlistCr.append("#")
    newlistCr.append(list[2])
    newlistCr.append("255")

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G = int(list[i * 3 + 5])
        B = int(list[i * 3 + 6])

        Y = 0.299 * R + 0.587 * G + 0.114 * B
        Cb = 0.564 * (B - Y)
        Cr = 0.713 * (R - Y)
        newlist.append(str((Y)))
        newlist.append(str((Cb)))
        newlist.append(str((Cr)))

        newlistY.append(str(int(Y)))
        newlistCb.append(str(int(Cb)))
        newlistCr.append(str(int(Cr)))

    listToWriteFile(newlistY, "Y - " + filename)
    listToWriteFile(newlistCb, "Cb - " + filename)
    listToWriteFile(newlistCr, "Cr - " + filename)

    return  newlist

#  вырезает каждую вторую строку, оставляя 0, 2, 4 и тд
def interlaced(list):
    all =[]
    lst =[]

    all.append(str(list[0]))
    all.append(str(list[1]))
    # all.append(str(list[2]))



    lst.extend(list[2].rstrip().split(' '))
    width = int(lst[0])  #колво столбцов
    height = int(lst[1]) #колво строк
    all.append(str(width)+" "+str(int(height/2)))
    all.append(str(list[3]))

    # i = 0
    # g = 0
    for i in range(int(len(list) / 3) - 1):
        g = (int(i/width))
        if g % 2 == 0:

            all.append(str(list[i * 3 + 4]))
            all.append(str(list[i * 3 + 5]))
            all.append(str(list[i * 3 + 6]))

    return all


#  вырезает каждый второй столбец, оставляя 0, 2, 4 и тд
def crosshead(list):
    all =[]
    lst =[]

    all.append(str(list[0]))
    all.append(str(list[1]))
    # all.append(str(list[2]))



    lst.extend(list[2].rstrip().split(' '))
    width = int(lst[0])  #колво столбцов
    height = int(lst[1]) #колво строк
    all.append(str(int(width/2))+" "+str(height))
    all.append(str(list[3]))

    # i = 0
    # g = 0
    for i in range(int(len(list) / 3) - 1):
        if i % 2 == 0:

            all.append(str(list[i * 3 + 4]))
            all.append(str(list[i * 3 + 5]))
            all.append(str(list[i * 3 + 6]))

    return all


def chroma_subsampling_4_4_2(list, filename):
    newlist = []
    newlist.append("P3")
    newlist.append("#")
    newlist.append(list[2])
    newlist.append("255")

    newlistY = []
    newlistY.append("P2")
    newlistY.append("#")
    newlistY.append(list[2])
    newlistY.append("255")

    newlistCb = []
    newlistCb.append("P2")
    newlistCb.append("#")
    newlistCb.append(list[2])
    newlistCb.append("255")

    newlistCr = []
    newlistCr.append("P2")
    newlistCr.append("#")
    newlistCr.append(list[2])
    newlistCr.append("255")

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G = int(list[i * 3 + 5])
        B = int(list[i * 3 + 6])

        Y = 0.299 * R + 0.587 * G + 0.114 * B
        Cb = 0.564 * (B - Y)
        Cr = 0.713 * (R - Y)
        newlist.append(str((Y)))
        newlist.append(str((Cb)))
        newlist.append(str((Cr)))

        newlistY.append(str(int(Y)))
        newlistCb.append(str(int(Cb)))
        newlistCr.append(str(int(Cr)))

    newlistY_chroma_subsampling = interlaced(newlistY)
    newlistCb_chroma_subsampling = interlaced(newlistCb)
    newlistCr_chroma_subsampling = interlaced(newlistCr)

    listToWriteFile(newlistY, "Y - chroma_subsampling_4_4_2 - " + filename)
    listToWriteFile(newlistCb_chroma_subsampling, "Cb - chroma_subsampling_4_4_2 - " + filename)
    listToWriteFile(newlistCr_chroma_subsampling, "Cr - chroma_subsampling_4_4_2 - " + filename)

    return  newlist

def chroma_subsampling_4_2_2(list, filename):
    newlist = []
    newlist.append("P3")
    newlist.append("#")
    newlist.append(list[2])
    newlist.append("255")

    newlistY = []
    newlistY.append("P2")
    newlistY.append("#")
    newlistY.append(list[2])
    newlistY.append("255")

    newlistCb = []
    newlistCb.append("P2")
    newlistCb.append("#")
    newlistCb.append(list[2])
    newlistCb.append("255")

    newlistCr = []
    newlistCr.append("P2")
    newlistCr.append("#")
    newlistCr.append(list[2])
    newlistCr.append("255")

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G = int(list[i * 3 + 5])
        B = int(list[i * 3 + 6])

        Y = 0.299 * R + 0.587 * G + 0.114 * B
        Cb = 0.564 * (B - Y)
        Cr = 0.713 * (R - Y)
        newlist.append(str((Y)))
        newlist.append(str((Cb)))
        newlist.append(str((Cr)))

        newlistY.append(str(int(Y)))
        newlistCb.append(str(int(Cb)))
        newlistCr.append(str(int(Cr)))

    newlistY_chroma_subsampling = crosshead(newlistY)
    newlistCb_chroma_subsampling = crosshead(newlistCb)
    newlistCr_chroma_subsampling = crosshead(newlistCr)

    listToWriteFile(newlistY, "Y - chroma_subsampling_4_2_2 - " + filename)
    listToWriteFile(newlistCb_chroma_subsampling, "Cb - chroma_subsampling_4_2_2 - " + filename)
    listToWriteFile(newlistCr_chroma_subsampling, "Cr - chroma_subsampling_4_2_2 - " + filename)

    return  newlist

def chroma_subsampling_4_2_0(list, filename):
    newlist = []
    newlist.append("P3")
    newlist.append("#")
    newlist.append(list[2])
    newlist.append("255")

    newlistY = []
    newlistY.append("P2")
    newlistY.append("#")
    newlistY.append(list[2])
    newlistY.append("255")

    newlistCb = []
    newlistCb.append("P2")
    newlistCb.append("#")
    newlistCb.append(list[2])
    newlistCb.append("255")

    newlistCr = []
    newlistCr.append("P2")
    newlistCr.append("#")
    newlistCr.append(list[2])
    newlistCr.append("255")

    for i in range(int(len(list) / 3) - 1):
        R = int(list[i * 3 + 4])
        G = int(list[i * 3 + 5])
        B = int(list[i * 3 + 6])

        Y = 0.299 * R + 0.587 * G + 0.114 * B
        Cb = 0.564 * (B - Y)
        Cr = 0.713 * (R - Y)
        newlist.append(str((Y)))
        newlist.append(str((Cb)))
        newlist.append(str((Cr)))

        newlistY.append(str(int(Y)))
        newlistCb.append(str(int(Cb)))
        newlistCr.append(str(int(Cr)))

    newlistY_chroma_subsampling = interlaced(crosshead(newlistY))
    newlistCb_chroma_subsampling = interlaced(crosshead(newlistCb))
    newlistCr_chroma_subsampling = interlaced(crosshead(newlistCr))

    listToWriteFile(newlistY, "Y - chroma_subsampling_4_2_0 - " + filename)
    listToWriteFile(newlistCb_chroma_subsampling, "Cb - chroma_subsampling_4_2_0 - " + filename)
    listToWriteFile(newlistCr_chroma_subsampling, "Cr - chroma_subsampling_4_2_0 - " + filename)

    return  newlist

def main():
     # Чтение dataset изображений ppm (в той же папке, что и файл программы)

    import glob

    for filename in glob.glob('*.ppm'):


        lst = readFileToList(filename)
        new = convert_bgr_to_YCbCr(lst, filename)
        listToWriteFile(new, "YCbCr - "+filename)




        new = interlaced(lst)
        listToWriteFile(new, "interlaced - "+filename)



        new = crosshead(lst)
        listToWriteFile(new, "crosshead - "+filename)


        chroma_subsampling_4_4_2(lst,filename)


        chroma_subsampling_4_2_2(lst,filename)
        chroma_subsampling_4_2_0(lst,filename)


if __name__ == "__main__":
    main()